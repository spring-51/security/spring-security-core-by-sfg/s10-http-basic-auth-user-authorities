package com.udemy.sfg.s10httpbasicauth.security.filters;


import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;

public class RestUrlParamAuthenticationFilter extends AbstractRestAuthenticationFilter {

    public RestUrlParamAuthenticationFilter(RequestMatcher requiresAuthenticationRequestMatcher) {
        super(requiresAuthenticationRequestMatcher);
    }

    @Override
    protected String getPassword(HttpServletRequest httpServletRequest) {
        return httpServletRequest.getParameter("apiSecret"); // pass password in this url query param from test and postman
    }

    @Override
    protected String getUsername(HttpServletRequest httpServletRequest) {
        return httpServletRequest.getParameter("apiKey"); // pass username in this url query param from test and postman
    }
}
