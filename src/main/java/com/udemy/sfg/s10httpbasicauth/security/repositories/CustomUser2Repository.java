package com.udemy.sfg.s10httpbasicauth.security.repositories;

import com.udemy.sfg.s10httpbasicauth.security.dtos.CustomUser2;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CustomUser2Repository extends JpaRepository<CustomUser2,Integer> {
    // this will be used by spring to load user by username
    Optional<CustomUser2> findByUsername(String username);
}
