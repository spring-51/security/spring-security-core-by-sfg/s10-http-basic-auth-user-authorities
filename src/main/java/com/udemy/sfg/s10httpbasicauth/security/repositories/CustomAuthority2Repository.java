package com.udemy.sfg.s10httpbasicauth.security.repositories;

import com.udemy.sfg.s10httpbasicauth.security.dtos.CustomAuthority2;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomAuthority2Repository extends JpaRepository<CustomAuthority2,Integer> {
}
