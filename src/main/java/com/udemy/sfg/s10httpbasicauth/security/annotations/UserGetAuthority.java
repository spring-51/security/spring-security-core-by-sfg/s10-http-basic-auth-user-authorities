package com.udemy.sfg.s10httpbasicauth.security.annotations;

import org.springframework.security.access.prepost.PreAuthorize;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(value = RetentionPolicy.RUNTIME)
@PreAuthorize("hasAnyAuthority('user.get')")
public @interface UserGetAuthority {
}
