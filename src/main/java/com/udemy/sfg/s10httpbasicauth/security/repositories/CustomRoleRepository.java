package com.udemy.sfg.s10httpbasicauth.security.repositories;

import com.udemy.sfg.s10httpbasicauth.security.dtos.CustomRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomRoleRepository extends JpaRepository<CustomRole, Integer> {
}
