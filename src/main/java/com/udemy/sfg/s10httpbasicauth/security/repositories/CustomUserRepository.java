package com.udemy.sfg.s10httpbasicauth.security.repositories;

import com.udemy.sfg.s10httpbasicauth.security.dtos.CustomUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CustomUserRepository extends JpaRepository<CustomUser, Integer> {
    // this will be used by spring to load user by username
    Optional<CustomUser> findByUsername(String username);
}
