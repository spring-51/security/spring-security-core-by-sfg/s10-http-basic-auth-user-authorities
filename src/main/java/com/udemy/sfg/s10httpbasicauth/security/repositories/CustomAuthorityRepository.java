package com.udemy.sfg.s10httpbasicauth.security.repositories;

import com.udemy.sfg.s10httpbasicauth.security.dtos.CustomAuthority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomAuthorityRepository extends JpaRepository<CustomAuthority, Integer> {
}
