package com.udemy.sfg.s10httpbasicauth.security.dtos;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
public class CustomRole {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;

    @ManyToMany(mappedBy = "roles")
    private Set<CustomUser> users;

    @Singular
    @ManyToMany(
            cascade = {CascadeType.MERGE, CascadeType.PERSIST},
            fetch = FetchType.EAGER // [Optional] its kept here to improve performance
    )
    @JoinTable(
            name = "role_authorities",
            joinColumns = {
                    @JoinColumn(name = "ROLE_ID",referencedColumnName = "ID") // what is  referencedColumnName ?
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "AUTHORITY_ID", referencedColumnName = "ID")
            }
    )
    private Set<CustomAuthority> authorities;
}
