package com.udemy.sfg.s10httpbasicauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S10HttpBasicAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(S10HttpBasicAuthApplication.class, args);
    }
}
