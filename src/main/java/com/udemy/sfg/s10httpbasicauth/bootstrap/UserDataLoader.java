package com.udemy.sfg.s10httpbasicauth.bootstrap;

import com.udemy.sfg.s10httpbasicauth.security.dtos.*;
import com.udemy.sfg.s10httpbasicauth.security.repositories.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@Component
@RequiredArgsConstructor
public class UserDataLoader implements CommandLineRunner {

    private final CustomUserRepository userRepository;
    private final CustomUser2Repository user2Repository;
    private final CustomAuthorityRepository authorityRepository;
    private final CustomAuthority2Repository authority2Repository;
    private final PasswordEncoder passwordEncoder;
    private final CustomRoleRepository roleRepository;

    @Transactional
    @Override
    public void run(String... args) throws Exception {

        // commented since roles entity definition changed in L67
        /*
        CustomAuthority admin =  authorityRepository.save(CustomAuthority.builder()
                .permission("ROLE_ADMIN")
                .build()
        );

        CustomAuthority user = authorityRepository.save(CustomAuthority.builder()
                .permission("ROLE_USER")
                .build()
        );

        CustomAuthority customer = authorityRepository.save(CustomAuthority.builder()
                .permission("ROLE_CUSTOMER")
                .build()
        );
         */

        // CustomAuthority entity - start
        CustomAuthority adminAuthority = authorityRepository.save(
                CustomAuthority.builder()
                        .permission("authority.admin")
                        .build());

        CustomAuthority getCustomerAuthority = authorityRepository.save(
                CustomAuthority.builder()
                        .permission("customer.get")
                        .build());

        CustomAuthority createCustomerAuthority = authorityRepository.save(
                CustomAuthority.builder()
                        .permission("customer.create")
                        .build());

        CustomAuthority updateCustomerAuthority = authorityRepository.save(
                CustomAuthority.builder()
                        .permission("customer.update")
                        .build());

        CustomAuthority getUserAuthority = authorityRepository.save(
                CustomAuthority.builder()
                        .permission("user.get")
                        .build());

        CustomAuthority createUserAuthority = authorityRepository.save(
                CustomAuthority.builder()
                        .permission("user.create")
                        .build());

        CustomAuthority updateUserAuthority = authorityRepository.save(
                CustomAuthority.builder()
                        .permission("user.update")
                        .build());

        // CustomAuthority entity - end

        // CustomRole entity - start
        CustomRole adminRole = roleRepository.save(CustomRole.builder()
                .name("ADMIN")
                .build()) ;
        CustomRole customerRole = roleRepository.save(CustomRole.builder()
                .name("CUSTOMER")
                .build()) ;
        CustomRole userRole = roleRepository.save(CustomRole.builder()
                .name("USER")
                .build()) ;
        // associating authority to the roles - start
        adminRole.setAuthorities(new HashSet<>(Arrays.asList(
                adminAuthority,
                getCustomerAuthority, createCustomerAuthority, updateCustomerAuthority,
                getUserAuthority, createUserAuthority, updateUserAuthority
        )));

        customerRole.setAuthorities(new HashSet<>(Arrays.asList(
                getUserAuthority,createUserAuthority,updateUserAuthority,
                getCustomerAuthority, createCustomerAuthority, updateCustomerAuthority)));

        userRole.setAuthorities(new HashSet<>(Arrays.asList(
                getUserAuthority, createUserAuthority, updateUserAuthority)));

        roleRepository.saveAll(Arrays.asList(adminRole,customerRole,userRole));
        // associating authority to the roles - end
        // CustomRole entity - end

        // CustomUser entity - start

        CustomUser noopUser = CustomUser.builder()
                .role(adminRole)
                .username("noopuser")
                .password(passwordEncoder.encode("noopuser"))
                .build();

        CustomUser ldapUser = CustomUser.builder()
                .role(userRole)
                .username("ldapuser")
                .password(passwordEncoder.encode("ldapuser"))
                .build();

        CustomUser sha256User = CustomUser.builder()
                .role(customerRole)
                .username("sha256user")
                .password(passwordEncoder.encode("sha256user"))
                .build();

        CustomUser bcryptUser = CustomUser.builder()
                .role(customerRole)
                .username("bcryptuser")
                .password(passwordEncoder.encode("bcryptuser"))
                .build();

        CustomUser bcrypt15User = CustomUser.builder()
                .role(customerRole)
                .username("bcrypt15user")
                .password(passwordEncoder.encode("bcrypt15user"))
                .build();

        userRepository.saveAll(Arrays.asList(noopUser, ldapUser, sha256User, bcryptUser, bcrypt15User));
        // CustomUser entity - end

        // CustomUser2 entity - start

        CustomAuthority2 noAccess =authority2Repository.save(CustomAuthority2.builder().role("ROLE_NOACCESS").build());

        user2Repository.save(
                CustomUser2.builder()
                        .authority(noAccess)
                        .username("noopuser2")
                        .password(NoOpPasswordEncoder.getInstance().encode("noopuser2"))
                        .build()
        );

        // CustomUser2 entity - end
        log.debug("Users loaded ...............");

    }
}
