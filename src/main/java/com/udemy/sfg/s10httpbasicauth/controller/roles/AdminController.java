package com.udemy.sfg.s10httpbasicauth.controller.roles;

import com.udemy.sfg.s10httpbasicauth.security.annotations.UserGetAuthority;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/role/admins")
public class AdminController {

    @PreAuthorize("hasAnyAuthority('authority.admin')")
    @GetMapping
    public String getAPI(){
        return "Hello from AdminController -> getAPI";
    }

    // @Secured({"ROLE_ADMIN", "ROLE_USER"}) // commented since we are using authorities refer UserDataLoader -> run (...)
    @UserGetAuthority
    @GetMapping("/users")
    public String userAndCustomerAndAdminRoleAccessInAdmin(){
        return "Hello from AdminController -> userRoleAccessInAdmin";
    }

    // @PreAuthorize(value = "hasAnyRole('ROLE_ADMIN','CUSTOMER')") // commented since we are using authorities refer UserDataLoader -> run (...)
    @PreAuthorize(value = "hasAnyAuthority('customer.get')")
    @GetMapping("/customers")
    public String customerAndAdminRoleAccessInAdmin(){
        return "Hello from AdminController -> userRoleAccessInAdmin";
    }

}
