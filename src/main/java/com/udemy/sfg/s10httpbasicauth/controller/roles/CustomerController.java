package com.udemy.sfg.s10httpbasicauth.controller.roles;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/role/customers")
public class CustomerController {
    @PreAuthorize("hasAnyAuthority('customer.get')")
    @GetMapping
    public String getAPI(){
        return "Hello from CustomerController -> getAPI";
    }

    @PostMapping
    @PreAuthorize("hasAnyAuthority('customer.create')")
    public String createAPI(){
        return "Hello from CustomerController -> createAPI";
    }

    @PutMapping
    @PreAuthorize("hasAnyAuthority('customer.update')")
    public String updateAPI(){
        return "Hello from CustomerController -> updateAPI";
    }
}
