package com.udemy.sfg.s10httpbasicauth.controller.roles;

import com.udemy.sfg.s10httpbasicauth.security.annotations.UserGetAuthority;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/role/users")
public class UserController {

    @UserGetAuthority
    @GetMapping
    public String getAPI(){
        return "Hello from UserController -> getAPI";
    }

    @UserGetAuthority
    @GetMapping("/2")
    public String getAPI2(){
        return "Hello from UserController -> getAPI2";
    }

    @UserGetAuthority
    @GetMapping("/3")
    public String getAPI3(){
        return "Hello from UserController -> getAPI3";
    }

    @PreAuthorize("hasAnyAuthority('user.create')")
    @PostMapping
    public String createAPI(){
        return "Hello from UserController -> createAPI";
    }

    @PreAuthorize("hasAnyAuthority('user.update')")
    @PutMapping
    public String updateAPI(){
        return "Hello from UserController -> updateAPI";
    }
}
