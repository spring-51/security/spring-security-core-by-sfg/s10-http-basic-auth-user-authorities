package com.udemy.sfg.s10httpbasicauth.nocontroller;

import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.LdapShaPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.util.DigestUtils;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PassEncodingTests {

    private final static String PASSWORD= "password";

    @Test
    void hashingExampleTest(){
        // DigestUtils.md5DigestAsHex(PASSWORD.getBytes())
        // return hash value of string, it remains same string
        // hence converting password to hash and saving to db is not secure
        System.out.println(DigestUtils.md5DigestAsHex(PASSWORD.getBytes()));
        System.out.println(DigestUtils.md5DigestAsHex(PASSWORD.getBytes()));
        System.out.println(DigestUtils.md5DigestAsHex(PASSWORD.getBytes()));
        System.out.println(DigestUtils.md5DigestAsHex(PASSWORD.getBytes()));
        System.out.println(DigestUtils.md5DigestAsHex(PASSWORD.getBytes()));

    }

    @Test
    void saltingExampleTest(){
        // DigestUtils.md5DigestAsHex(PASSWORD.getBytes())
        // return hash value of string, it remains same string
        // hence converting password to hash and saving to db is not secure
        System.out.println(DigestUtils.md5DigestAsHex(PASSWORD.getBytes()));
        System.out.println(DigestUtils.md5DigestAsHex(PASSWORD.getBytes()));

        final String salted = PASSWORD + "MyLongSaltAsSuffix";

        System.out.println(DigestUtils.md5DigestAsHex(salted.getBytes()));
        System.out.println(DigestUtils.md5DigestAsHex(salted.getBytes()));

    }

    @Test
    void noopPasswordEncoderExampleTest(){
        PasswordEncoder noop = NoOpPasswordEncoder.getInstance();
        String encoded = noop.encode(PASSWORD);
        System.out.println(encoded);
    }

    @Test
    void ldapPasswordEncoderExampleTest() {
        PasswordEncoder ldap = new LdapShaPasswordEncoder();
        System.out.println(ldap.encode(PASSWORD));
        System.out.println(ldap.encode(PASSWORD));
        System.out.println(ldap.encode(PASSWORD));
        assertTrue(ldap.matches(PASSWORD, ldap.encode(PASSWORD)));
        assertTrue(ldap.matches(PASSWORD, PASSWORD));
    }

    @Test
    void sha256PasswordEncoderExampleTest() {
        PasswordEncoder sha256 = new StandardPasswordEncoder();
        System.out.println(sha256.encode(PASSWORD));
        System.out.println(sha256.encode(PASSWORD));
        System.out.println(sha256.encode(PASSWORD));
        assertTrue(sha256.matches(PASSWORD, sha256.encode(PASSWORD)));
        // assertFalse(sha256.matches(PASSWORD, PASSWORD));
    }

    @Test
    void bcryptPasswordEncoderExampleTest() {
        // stringLength = 10
        System.out.println("\n Test start for String length = 10 \n");
        PasswordEncoder bcrypt = new BCryptPasswordEncoder();
        long startTime = System.currentTimeMillis();
        System.out.println(bcrypt.encode(PASSWORD));
        System.out.println(bcrypt.encode(PASSWORD));

        assertTrue(bcrypt.matches(PASSWORD, bcrypt.encode(PASSWORD)));
        assertFalse(bcrypt.matches(PASSWORD, PASSWORD));
        System.out.println("Total time taken - "+(System.currentTimeMillis() - startTime));

        // stringLength = 12
        System.out.println("\n Test start for String length = 12 \n");
        bcrypt = new BCryptPasswordEncoder(12);
        startTime = System.currentTimeMillis();
        System.out.println(bcrypt.encode(PASSWORD));
        System.out.println(bcrypt.encode(PASSWORD));

        assertTrue(bcrypt.matches(PASSWORD, bcrypt.encode(PASSWORD)));
        assertFalse(bcrypt.matches(PASSWORD, PASSWORD));
        System.out.println("Total time taken - "+(System.currentTimeMillis() - startTime));

        // stringLength = 16
        System.out.println("\n Test start for String length = 16 \n");
        bcrypt = new BCryptPasswordEncoder(16);
        startTime = System.currentTimeMillis();
        System.out.println(bcrypt.encode(PASSWORD));
        System.out.println(bcrypt.encode(PASSWORD));

        assertTrue(bcrypt.matches(PASSWORD, bcrypt.encode(PASSWORD)));
        assertFalse(bcrypt.matches(PASSWORD, PASSWORD));
        System.out.println("Total time taken - "+(System.currentTimeMillis() - startTime));
    }
}
