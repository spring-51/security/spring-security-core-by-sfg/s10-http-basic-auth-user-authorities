package com.udemy.sfg.s10httpbasicauth.controller.roles;

import com.udemy.sfg.s10httpbasicauth.controller.BaseTests;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.stream.Stream;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class AdminControllerTests extends BaseTests {
    /*
     noopuser - is ROLE_ADMIN,
     ldapuser - is ROLE_USER,
     sha256user - ROLE_CUSTOMER,
     bcryptuser - ROLE_CUSTOMER,
     bcrypt15user - ROLE_CUSTOMER

     refer - com.udemy.sfg.s09httpbasicauth.bootstrap.UserDataLoader
       -- there we are assigning role to above specified user
     */

    private static final String ADMIN_ROLE_USER_USERNAME= "noopuser";
    private static final String ADMIN_ROLE_USER_PASSWORD= ADMIN_ROLE_USER_USERNAME;

    private static final String CUSTOMER_ROLE_USER_USERNAME= "sha256user";
    private static final String CUSTOMER_ROLE_USER_PASSWORD= CUSTOMER_ROLE_USER_USERNAME;

    private static final String USER_ROLE_USER_USERNAME= "ldapuser";
    private static final String USER_ROLE_USER_PASSWORD= USER_ROLE_USER_USERNAME;

    private static final String INCORRECT_USERNAME= "incorrectusername";
    private static final String INCORRECT_PASSWORD= INCORRECT_USERNAME;

    // getAPI - start

    @Nested
    @DisplayName("getAPI api tests")
    class GetAPITests{

        @Test
        void testGetAPI_AS_AdminRoleUser_ValidCredential_200() throws Exception{
            mockMvc.perform(get("/role/admins")
                    .with(httpBasic(ADMIN_ROLE_USER_USERNAME, ADMIN_ROLE_USER_PASSWORD)))
                    .andExpect(status().isOk());
        }


        @ParameterizedTest(name = "#{index} with [{arguments}]")
        @MethodSource("com.udemy.sfg.s10httpbasicauth.controller.roles.AdminControllerTests#getStreamAllUnauthorizedRoleUsers") // packageName.className#staticMethodToSupplyArgs
        void testGetAPI_ValidCredential_403(String username, String password) throws Exception{
            mockMvc.perform(get("/role/admins")
                    .with(httpBasic(username, password)))
                    .andExpect(status().isForbidden()); // For role related restriction we  get 403 - Forbidden
        }

        @Test
        void testGetAPI_InvalidCredential_401() throws Exception{
            mockMvc.perform(get("/role/admins")
                    .with(httpBasic(INCORRECT_USERNAME, INCORRECT_PASSWORD)))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void testGetAPI_NoAuth_401() throws Exception {
            mockMvc.perform(get("/role/admins"))
                    .andExpect(status().isUnauthorized());
        }
    }
    // getAPI - end
    // userAndAdminRoleAccessInAdmin - start

    @Nested
    @DisplayName("userAndAdminRoleAccessInAdmin api tests")
    class UserAndAdminRoleAccessInAdminApi{

        @ParameterizedTest(name = "#{index} with [{arguments}]")
        @MethodSource("com.udemy.sfg.s10httpbasicauth.controller.roles.AdminControllerTests#getStreamAllMethodLevelAuthorizedRoleUsers") // packageName.className#staticMethodToSupplyArgs
        void userAndAdminRoleAccessInAdmin_MethodLevelAccess_200( String username, String password) throws Exception{
            mockMvc.perform(get("/role/admins/users")
                    .with(httpBasic(username, password)))
                    .andExpect(status().isOk());
        }

        @Test
        void userAndCustomerAndAdminRoleAccessInAdmin_AS_CustomerRoleUser_MethodLevelAccess_403() throws Exception{
            mockMvc.perform(get("/role/admins/users")
                    .with(httpBasic(CUSTOMER_ROLE_USER_USERNAME, CUSTOMER_ROLE_USER_PASSWORD)))
                    //.andExpect(status().isForbidden()); // Customer has all the user access - refer UserDataLoader-> run ()
                    .andExpect(status().isOk());
        }
    }
    // userAndAdminRoleAccessInAdmin - end

    // customerAndAdminRoleAccessInAdmin - start
    @DisplayName("customerAndAdminRoleAccessInAdmin api tests")
    @Nested
    class CustomerAndAdminRoleAccessInAdminApiTests{
        @ParameterizedTest(name = "#{index} with [{arguments}]")
        @MethodSource("com.udemy.sfg.s10httpbasicauth.controller.roles.AdminControllerTests#getStreamAllMethodLevelAuthorizedRoleUsers2") // packageName.className#staticMethodToSupplyArgs
        void customerAndAdminRoleAccessInAdmin_MethodLevelAccess_200( String username, String password) throws Exception{
            mockMvc.perform(get("/role/admins/customers")
                    .with(httpBasic(username, password)))
                    .andExpect(status().isOk());
        }

        @Test
        void customerAndAdminRoleAccessInAdmin_AS_CustomerRoleUser_MethodLevelAccess_403() throws Exception{
            mockMvc.perform(get("/role/admins/customers")
                    .with(httpBasic(USER_ROLE_USER_USERNAME, USER_ROLE_USER_PASSWORD)))
                    .andExpect(status().isForbidden());
        }
    }
    // customerAndAdminRoleAccessInAdmin - end

    private static Stream<Arguments> getStreamAllUnauthorizedRoleUsers() {
        return Stream.of(
                Arguments.of(CUSTOMER_ROLE_USER_USERNAME, CUSTOMER_ROLE_USER_PASSWORD),
                Arguments.of(USER_ROLE_USER_USERNAME, USER_ROLE_USER_PASSWORD)
        );
    }

    private static Stream<Arguments> getStreamAllMethodLevelAuthorizedRoleUsers() {
        return Stream.of(
                Arguments.of(ADMIN_ROLE_USER_USERNAME, ADMIN_ROLE_USER_PASSWORD),
                Arguments.of(USER_ROLE_USER_USERNAME, USER_ROLE_USER_PASSWORD)
        );
    }

    private static Stream<Arguments> getStreamAllMethodLevelAuthorizedRoleUsers2() {
        return Stream.of(
                Arguments.of(ADMIN_ROLE_USER_USERNAME, ADMIN_ROLE_USER_PASSWORD),
                Arguments.of(CUSTOMER_ROLE_USER_USERNAME, CUSTOMER_ROLE_USER_PASSWORD)
        );
    }


}
