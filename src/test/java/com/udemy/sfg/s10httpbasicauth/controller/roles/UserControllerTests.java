package com.udemy.sfg.s10httpbasicauth.controller.roles;

import com.udemy.sfg.s10httpbasicauth.controller.BaseTests;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.stream.Stream;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class UserControllerTests extends BaseTests {

    /*
     noopuser - is ROLE_ADMIN,
     ldapuser - is ROLE_USER,
     sha256user - ROLE_CUSTOMER,
     bcryptuser - ROLE_CUSTOMER,
     bcrypt15user - ROLE_CUSTOMER

     noopuser2 - ROLE_NOACCESS

       refer - com.udemy.sfg.s09httpbasicauth.bootstrap.UserDataLoader
       -- there we are assigning role to above specified user
     */

    private static final String ADMIN_ROLE_USER_USERNAME= "noopuser";
    private static final String ADMIN_ROLE_USER_PASSWORD= ADMIN_ROLE_USER_USERNAME;

    private static final String CUSTOMER_ROLE_USER_USERNAME= "sha256user";
    private static final String CUSTOMER_ROLE_USER_PASSWORD= CUSTOMER_ROLE_USER_USERNAME;

    private static final String USER_ROLE_USER_USERNAME= "ldapuser";
    private static final String USER_ROLE_USER_PASSWORD= USER_ROLE_USER_USERNAME;

    // ths user is stored in CustomUser2 entity
    private static final String NOACCESS_ROLE_USER_USERNAME= "noopuser2";
    private static final String NOACCESS_ROLE_USER_PASSWORD= NOACCESS_ROLE_USER_USERNAME;

    private final String INCORRECT_USERNAME= "incorrectusername";
    private final String INCORRECT_PASSWORD= INCORRECT_USERNAME;


    // testGetAPI - start

    @DisplayName("testGetAPI - tests")
    @Nested
    class GetAPITests{
        @ParameterizedTest(name = "#{index} with [{arguments}]")
        @MethodSource("com.udemy.sfg.s10httpbasicauth.controller.roles.UserControllerTests#getStreamAllUsers") // packageName.className#staticMethodToSupplyArgs
        void testGetAPI_ValidCredential_200(String user, String pwd) throws Exception {
            mockMvc.perform(get("/role/users").with(httpBasic(user, pwd)))
                    .andExpect(status().isOk());
        }

        @Test
        void testGetAPI_AS_NoAccessRoleUser_ValidCredential_403() throws Exception {
            mockMvc.perform(get("/role/users").with(httpBasic(NOACCESS_ROLE_USER_USERNAME, NOACCESS_ROLE_USER_PASSWORD)))
                    .andExpect(status().isForbidden());
        }

        @Test
        void testGetAPI_NoAuth_401() throws Exception {
            mockMvc.perform(get("/role/users"))
                    .andExpect(status().isUnauthorized());
        }
    }

    // testGetAPI - end

    // testGetAPI2 - start

    @DisplayName("testGetAPI2 - tests")
    @Nested
    class GetAPI2Tests{
        @ParameterizedTest(name = "#{index} with [{arguments}]")
        @MethodSource("com.udemy.sfg.s10httpbasicauth.controller.roles.UserControllerTests#getStreamAllUsers") // packageName.className#staticMethodToSupplyArgs
        void testGetAPI2_ValidCredential_200(String user, String pwd) throws Exception {
            mockMvc.perform(get("/role/users/2").with(httpBasic(user, pwd)))
                    .andExpect(status().isOk());
        }

        @Test
        void testGetAPI2_AS_NoAccessRoleUser_ValidCredential_403() throws Exception {
            mockMvc.perform(get("/role/users/2").with(httpBasic(NOACCESS_ROLE_USER_USERNAME, NOACCESS_ROLE_USER_PASSWORD)))
                    .andExpect(status().isForbidden());
        }

        @Test
        void testGetAPI2_NoAuth_401() throws Exception {
            mockMvc.perform(get("/role/users2"))
                    .andExpect(status().isUnauthorized());
        }
    }

    // testGetAPI2 - end

    // testGetAPI3 - start

    @DisplayName("testGetAPI3 - tests")
    @Nested
    class GetAPI3Tests{
        @ParameterizedTest(name = "#{index} with [{arguments}]")
        @MethodSource("com.udemy.sfg.s10httpbasicauth.controller.roles.UserControllerTests#getStreamAllUsers") // packageName.className#staticMethodToSupplyArgs
        void testGetAPI3_ValidCredential_200(String user, String pwd) throws Exception {
            mockMvc.perform(get("/role/users/3").with(httpBasic(user, pwd)))
                    .andExpect(status().isOk());
        }

        @Test
        void testGetAPI3_AS_NoAccessRoleUser_ValidCredential_403() throws Exception {
            mockMvc.perform(get("/role/users/3").with(httpBasic(NOACCESS_ROLE_USER_USERNAME, NOACCESS_ROLE_USER_PASSWORD)))
                    .andExpect(status().isForbidden());
        }

        @Test
        void testGetAPI3_NoAuth_401() throws Exception {
            mockMvc.perform(get("/role/users/3"))
                    .andExpect(status().isUnauthorized());
        }
    }

    // testGetAPI3 - end

    @DisplayName("createAPI - tests")
    @Nested
    class createAPITests{

        @ParameterizedTest(name = "#{index} with [{arguments}]")
        @MethodSource("com.udemy.sfg.s10httpbasicauth.controller.roles.UserControllerTests#getStreamAllUsers") // packageName.className#staticMethodToSupplyArgs
        void createAPI_ValidCredential_200(String user, String pwd) throws Exception {
            mockMvc.perform(post("/role/users").with(httpBasic(user, pwd)))
                    .andExpect(status().isOk());
        }

        @Test
        void createAPI_AS_NoAccessRoleUser_ValidCredential_403() throws Exception {
            mockMvc.perform(post("/role/users").with(httpBasic(NOACCESS_ROLE_USER_USERNAME, NOACCESS_ROLE_USER_PASSWORD)))
                    .andExpect(status().isForbidden());
        }

        @Test
        void createAPI_NoAuth_401() throws Exception {
            mockMvc.perform(post("/role/users"))
                    .andExpect(status().isUnauthorized());
        }
    }

    @DisplayName("updateAPI - tests")
    @Nested
    class updateAPITests{

        @ParameterizedTest(name = "#{index} with [{arguments}]")
        @MethodSource("com.udemy.sfg.s10httpbasicauth.controller.roles.UserControllerTests#getStreamAllUsers") // packageName.className#staticMethodToSupplyArgs
        void updateAPI_ValidCredential_200(String user, String pwd) throws Exception {
            mockMvc.perform(put("/role/users").with(httpBasic(user, pwd)))
                    .andExpect(status().isOk());
        }

        @Test
        void updateAPI_AS_NoAccessRoleUser_ValidCredential_403() throws Exception {
            mockMvc.perform(put("/role/users").with(httpBasic(NOACCESS_ROLE_USER_USERNAME, NOACCESS_ROLE_USER_PASSWORD)))
                    .andExpect(status().isForbidden());
        }

        @Test
        void updateAPI_NoAuth_401() throws Exception {
            mockMvc.perform(put("/role/users"))
                    .andExpect(status().isUnauthorized());
        }
    }

    private static Stream<Arguments> getStreamAllUsers() {
        return Stream.of(
                Arguments.of(ADMIN_ROLE_USER_USERNAME , ADMIN_ROLE_USER_PASSWORD),
                Arguments.of(CUSTOMER_ROLE_USER_USERNAME, CUSTOMER_ROLE_USER_PASSWORD),
                Arguments.of(USER_ROLE_USER_USERNAME, USER_ROLE_USER_PASSWORD));
    }
}
